<?php

namespace app\models;


use app\core\DBase;

class Board
{
    
    static function addSecondPlayer($gameId){
        mysqli_query(DBase::getInstance()->getLink(), "UPDATE board SET `player2` = '" . Player::getPlayerId() . "' WHERE `id` = {$gameId}");
    }

    static function getOpenBoards(){
        $arr = [];
        $query = mysqli_query(DBase::getInstance()->getLink(), "SELECT * FROM board WHERE `player2` = ''");

        while ($row = mysqli_fetch_assoc($query)) {
            $arr[] = $row['id'];
        }

        return $arr;

    }

    static function createGame(){

        mysqli_query(DBase::getInstance()->getLink(), "INSERT INTO board (`player1`) VALUES ('" . Player::getPlayerId() . "')");

    }

    static function getCreatingDate($gameId){
        $query = mysqli_query(DBase::getInstance()->getLink(), "SELECT `timestamp` FROM board WHERE `id` = '{$gameId}'");

        return $query;
    }

    static function getFirstPlayerId($gameId){
        $query = mysqli_query(DBase::getInstance()->getLink(), "SELECT `player1` FROM board WHERE `id` = '{$gameId}'");

        return mysqli_fetch_assoc($query);
    }
    
}