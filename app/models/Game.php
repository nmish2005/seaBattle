<?php

namespace app\models;

use app\core\DBase;

class Game
{

    private $game = [];

    function __construct()
    {
        $this->initGame();
    }

    function initGame()
    {
        $game = [];

        for ($i = 0; $i < 10; $i++) {
            $game[$i] = [];

            for ($j = 0; $j < 10; $j++) {

                $game[$i][$j] = 0;

            }

        }

        $this->game = $game;
    }

    function randRotate($arr)
    {

        $randRotation = mt_rand(1, 3);

        for ($i = 0; $i < $randRotation; $i++) {
            $arr = $this->rotateMatrixBy90Deg($arr);
        }

        return $arr;

    }

    function newShip($type)
    {
        $ship = array();

        for ($i = 0; $i < $type + 2; $i++) {
            $ship[$i] = array();
            for ($j = 0; $j < 3; $j++) {
                $ship[$i][$j] = 0;
            }
        }

        for ($i = 0; $i < count($ship); $i++) {
            for ($j = 0; $j < count($ship[$i]); $j++) {
                if (
                    isset($ship[$i - 1][$j])
                    && isset($ship[$i + 1][$j])
                    && isset($ship[$i][$j - 1])
                    && isset($ship[$i][$j + 1])
                ) {
                    $ship[$i][$j] = 1;
                }
            }
        }

        return $this->randRotate($ship);
    }

    function canInsert($x = 0, $y = 0, $arr)
    {

        //TODO: canInsert

    }

    function insertShip($x = 0, $y = 0, $arr)
    {

        //TODO: insertShip
        $i2 = 0;
        $j2 = 0;

        for ($i = $x; $i < 10; $i++) {
            for ($j = $y; $j < 10; $j++) {
                echo $i2 . $j2;
                $this->game[$i][$j] = $arr[$i2][$j2];
                $j2 = $j2 > 8 ? 0 : $j2 + 1;
            }
            $i2 = $i2 > 8 ? 0 : $i2 + 1;
        }
    }

    function rotateMatrixBy90Deg($matrix)
    {
        $rotatedMatrix = array();

        // make each new row = reversed old column
        foreach (array_keys($matrix[0]) as $column) {
            $rotatedMatrix[] = array_reverse(array_column($matrix, $column));
        }

        return $rotatedMatrix;

    }

    static function echoTable($matrix)
    {
        $table = "<table class='matrix_dump' style='border: 3px solid #000000; border-collapse: collapse; text-align: center'>";

        for ($i = 0; $i < count($matrix); $i++) {
            $table .= "<tr>";

            for ($j = 0; $j < count($matrix[$i]); $j++) {

                $table .= "<td style='border: 1px solid darkgray; width: 40px; height: 40px'>{$matrix[$i][$j]}</td>";

            }

            $table .= "</tr>";

        }

        $table .= "</table>";

        echo $table;

        return $table;

    }

    function getGameSituation()
    {
        return $this->game;
    }

    static function connectToGame($gameId)
    {

        if (!in_array($gameId, Board::getOpenBoards()))
            return false;

        if(Board::getFirstPlayerId($gameId) == Player::getPlayerId())
            return false;

        Board::addSecondPlayer($gameId);

        return true;

    }

    static function validateGameCreating()
    {
        return true;
    }

}