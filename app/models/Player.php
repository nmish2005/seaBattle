<?php

namespace app\models;


use app\core\DBase;

class Player
{

    static function getPlayerId(){
        if(!isset($_COOKIE['player'])) {

            $randString = "";
            $characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
            $max = count($characters) - 1;
            for ($i = 0; $i < 32; $i++) {
                $rand = mt_rand(0, $max);
                $randString .= $characters[$rand];
            }

            $playerId = $randString;
            setcookie("player", $playerId, time() + 86400, "/");
        }   else    {
            $playerId = $_COOKIE['player'];
        }

        return $playerId;
    }

    static function isMoveAllowed($playerId, $gameId)
    {

        $query = mysqli_query(DBase::getInstance()->getLink(), "SELECT * FROM `moves` WHERE `gameid` = '{$gameId}' ORDER BY `id` DESC LIMIT 1");

        $row_cnt = mysqli_num_rows($query);

        if ($row_cnt == 0 and $playerId == 2)
            return false;

        $row = mysqli_fetch_assoc($query);

        if ($row["player"] != $playerId) {

            if ($row["state"] == 1 or $row["state"] == 2) {

                return false;

            }

        } elseif ($row["state"] == 0) {

            return false;

        }

        if ($playerId == 1) {

            return true;

        }

        return false;

    }

}