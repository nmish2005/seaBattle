<?php

namespace app\controllers;


use app\core\App;
use app\core\Controller;
use app\models\Board;
use app\models\Game;

class PlayController extends Controller
{

    function action_index()
    {

        $this->View->render('play/index');

    }

    function action_connect()
    {

        Game::connectToGame(App::getUriComponents()[1]);

        $this->View->render('play/game', [
            'player' => 2
        ]);

    }

    function action_create()
    {

        if (!Game::validateGameCreating())
            $this->View->render('errors/gameCreatingLimit');

        Board::createGame();

        $this->View->render('play/game', [
            'player' => 1
        ]);

    }

}