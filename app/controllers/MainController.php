<?php

namespace app\controllers;


use app\core\Controller;

class MainController extends Controller
{

    function action_index()
    {
        $this->View->render('main');
    }

    function action_404()
    {
        $this->View->render('errors/404');
    }

    function action_test()
    {
        phpinfo();
    }
}