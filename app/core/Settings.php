<?php

namespace app\core;


class Settings
{

    private static $_instance = null;
    private $settings = [
        'projectName' => 'Морской бой',
        'projectUrl' => 'http://mike.sudoku.in.ua/seaBattle/',
        'css' => [
            'main',
            '../libs/bs/css/bootstrap.min',
            '../libs/animate',
            '../libs/fa/css/font-awesome.min',
            '../libs/swal2/sweetalert2.min'
        ],
        'js' => [
            '../libs/bs/css/bootstrap.min',
            '../libs/jq',
            '../libs/popper',
            '../libs/swal2/sweetalert2.min'
        ]
    ];

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    static function getInstance()
    {
        if (is_null(self::$_instance))
            self::$_instance = new self;

        return self::$_instance;
    }

    function getParamFromSettings($paramName)
    {
        return isset($this->settings[$paramName]) ? $this->settings[$paramName] : null;
    }

    function param($paramName, $value = null)
    {
        if (is_null($value)) {
            return isset($this->settings[$paramName]) ? $this->settings[$paramName] : null;
        }

        return $this->settings[$paramName] = $value;
    }
}