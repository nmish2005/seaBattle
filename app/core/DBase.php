<?php

namespace app\core;

class DBase
{

    private $link;
    private static $_instance = null;

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    private function __construct()
    {
        include_once $_SERVER['DOCUMENT_ROOT'] . '/dbSettings.php';

        $this->link = mysqli_connect($dbSettings['host'], $dbSettings['user'], $dbSettings['pass'], $dbSettings['db']) or die(
            "<h1 style='color: crimson'>DB ERROR (" . mysqli_error($this->link) . "; no " . mysqli_errno($this->link) . ")</h1>"
        );
    }

    function getLink()
    {
        return $this->link;
    }

    static function getInstance()
    {
        if (is_null(self::$_instance))
            self::$_instance = new self;

        return self::$_instance;
    }
}