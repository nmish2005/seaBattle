<?php

namespace app\core;


class Router
{

    static function Launch()
    {

        $uriComponents = App::getUriComponents();
        if($uriComponents[0] == '') {
            unset($uriComponents[0]);
            $uriComponents = array_values($uriComponents);
        }
        $requestUri = explode('/seaBattle', $_SERVER['REQUEST_URI'])[1];

        $query = mysqli_query(DBase::getInstance()->getLink(), "SELECT `controller`, `action` FROM aliases WHERE `name` = '{$requestUri}' LIMIT 1");

        $query = mysqli_fetch_assoc($query);

        if (is_null($query)) {

            $controllerName = isset($uriComponents[0]) ? $uriComponents[0] : 'main';
            $actionName = isset($uriComponents[1]) ? $uriComponents[1] : 'index';

        } else {

            $controllerName = $query['controller'];
            $actionName = $query['action'];

        }
        $controllerName = ucfirst($controllerName) . 'Controller';
        $actionName = 'action_' . strtolower($actionName);

        $controllerPath = 'app\\controllers\\' . $controllerName;

        if (class_exists($controllerPath))
            $controller = new $controllerPath;
        else
            return Router::ErrorPage404();

        if (method_exists($controller, $actionName))
            $controller->$actionName();
        else
            return Router::ErrorPage404();

        return true;
    }

    static function ErrorPage404()
    {

        http_response_code(404);

        $mainController = new \app\controllers\MainController;

        $mainController->action_404();

        return 404;
    }

}