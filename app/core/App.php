<?php

namespace app\core;


class App
{

    static function addCss($arr)
    {
        Settings::getInstance()->param(
            'css',
            array_merge(
                Settings::getInstance()->param('css'),
                (array)$arr
            )
        );

        return true;
    }

    static function addJs($arr)
    {
        Settings::getInstance()->param(
            'js',
            array_merge(
                Settings::getInstance()->param('js'),
                (array)$arr
            )
        );

        return true;
    }

    static function genCss()
    {
        $output = '';

        foreach (Settings::getInstance()->param('css') as $item) {
            $output .= '<link rel="stylesheet" href="/seaBattle/assets/css/' . $item . '.css">';
        }

        return $output;
    }

    static function genJs()
    {
        $output = '';

        foreach (Settings::getInstance()->param('js') as $item) {
            $output .= '<script src="/seaBattle/assets/js/' . $item . '.js"></script>';
        }

        return $output;
    }

    static function getUriComponents(){
        $uriComponents = explode('/', $_SERVER['REQUEST_URI']);

        unset($uriComponents[0]);
        unset($uriComponents[1]);

        $uriComponents = array_values($uriComponents);

        return $uriComponents;
    }

}