<?php

namespace app\views;

use app\models\Board;

?>
<button onclick="location='/seaBattle/create'" class="btn btn-outline-primary">Создать игру</button>
<br><br>
<?php

$games = Board::getOpenBoards();

foreach ($games as $item) {
    echo "<button class='btn btn-outline-success' onclick=\"location='/seaBattle/connect/{$item}'\"> Играть (игра #{$item}) </button><br>";
}