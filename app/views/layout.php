<?php

namespace app\views;

use app\core\App;

?>

<!DOCTYPE html>
<html>
<head>
    <title>Морской бой</title>
    <meta charset="UTF-8">
    <?=App::genCss()?>
</head>
<body>
<div class="container">
    <?php
    include_once $_SERVER['DOCUMENT_ROOT'] . '/seaBattle/app/views/' . $fileName . '.php';
    ?>
</div>

<?=App::genJs()?>
</body>
</html>