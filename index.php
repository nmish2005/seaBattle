<?php

error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_errors', 1);

include_once 'vendor/autoload.php';
include_once 'app/bootstrap.php';